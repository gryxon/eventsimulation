﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQueue
{
    interface PriorityQueue<T> // Interfejs dla kolejki priorytetowej.
    {

        void InitQueue(T[] elems, double[] ranks); // Tworzy kolejkę priorytetową z podanych danych.
        void InsertElement(T elem, double rank); // Dodaje element do kolejki.
        T GetFirst(); // Pobiera element o najwyższej randze z kolejki.
        T EraseFirst(); // Pobiera element o najwyższej randze oraz go usuwa z kolejki.

       // bool compare(int indexOfElement1, int indexOfElement2); // Funkcja porównująca nasze elementy. Może się przydać później. 
        
    }
}
