﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQueue
{
    class Program
    {
        private static QueueTest test;

        static void Main(string[] args)
        {
            int A, B, N, M;

            M = Convert.ToInt32(Console.ReadLine());
            N = Convert.ToInt32(Console.ReadLine());
            A = Convert.ToInt32(Console.ReadLine());
            B = Convert.ToInt32(Console.ReadLine());


            test = new QueueTest(A, B, M, N);

            //test.GenerateDataToGraph();

            double X, Y;

            X = test.ResultOfTest(0);
            Y = test.ResultOfTest(1);

            Console.Write(X + " " + Y + "\n");

            Console.Write(" ");
            
            Console.ReadKey();   // opcjonalne
        }
    }
}
