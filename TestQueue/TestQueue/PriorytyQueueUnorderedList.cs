﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQueue
{
    class PriorytyQueueUnorderedList<T> : PriorityQueue<T>
    {
        private Node<T>[] nodes;
        private int numberOfElem;
        private long currentId;

        public PriorytyQueueUnorderedList()
        {
            numberOfElem = 0;
            nodes = new Node<T>[1048576 + 2];
            currentId = 0;
        }

        public void InsertElement(T elem, double rank)
        {
            nodes[numberOfElem++] = new Node<T>(currentId++, rank, elem);
        }

        public T GetFirst()
        {
            double x = nodes[0].Rank;
            int index = 0;
            long id = nodes[0].Id;
            for (int i = 0; i < numberOfElem; i++)
            {
                if (nodes[i].Rank < x)
                {
                    x = nodes[i].Rank;
                    index = i;
                    id = nodes[i].Id;
                }
                else if (nodes[i].Rank == x && nodes[i].Id < id)
                {
                    x = nodes[i].Rank;
                    index = i;
                    id = nodes[i].Id;
                }
            }
            return nodes[index].Elem;
        }

        public T EraseFirst()
        {
            double x = nodes[0].Rank;
            int index = 0;
            long id = nodes[0].Id;
            for (int i = 0; i < numberOfElem; i++)
            {
                if (nodes[i].Rank < x)
                {
                    x = nodes[i].Rank;
                    index = i;
                    id = nodes[i].Id;
                }
                else if (nodes[i].Rank == x && nodes[i].Id < id)
                {
                    x = nodes[i].Rank;
                    index = i;
                    id = nodes[i].Id;
                }
            }

            T result = nodes[index].Elem;
            Swap(ref nodes[index], ref nodes[numberOfElem - 1]);
            --numberOfElem;

            return result;
        }


        public void InitQueue(T[] elems, double[] ranks)
        {
            for (int i = 0; i < ranks.Length; ++i)
            {
                InsertElement(elems[i], ranks[i]);
            }
        }


        private void Swap<K>(ref K lhs, ref K rhs)
        {
            K temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

    }

}