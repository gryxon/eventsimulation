﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestQueue
{
    class QueueTest
    {
        private Random generator = new Random();
        private Stopwatch stopWatch = new Stopwatch();

        private int a,b,m,n;

        PriorityQueueTournamentTree<int> tree = new PriorityQueueTournamentTree<int>();
        PriorytyQueueUnorderedList<int> list = new PriorytyQueueUnorderedList<int>();

        public QueueTest(int a, int b, int m, int n)
        {
            this.a = a;
            this.b = b;
            this.m = m;
            this.n = n;
        }

        public void setParameters(int a, int b, int m, int n)
        {
            this.a = a;
            this.b = b;
            this.m = m;
            this.n = n;
        }

        private double MakeTest(PriorityQueue<int> queue) 
        {
            stopWatch.Reset();
            stopWatch.Start();

            for (int i = 0; i < a; ++i) queue.InsertElement(i, (-1) * generator.Next(1, m)); // Aby podawało element o największym klucz.

            for (int i = 0; i < b; ++i)
            {
                queue.InsertElement(i, (-1) * generator.Next(1, n));
                queue.EraseFirst();
            }

            stopWatch.Stop();

            double result = stopWatch.ElapsedMilliseconds;


            return result;
        }

        public double ResultOfTest(int type) // 0 - lista; wszystko inne drzewo
        {
            double result;
            if (type == 0)
            {
                result = MakeTest(list);
                return result;
            }
            else
            {
                result = MakeTest(tree);
                return result;
            }
        }

        public void GenerateDataToGraph()
        {
            StreamWriter writer = new StreamWriter("danewykres.txt");
            writer.Write("[");
            int tmpA = a;
            int tmpB = b;
            Console.Write("Generowanie danych dla listy: \n");
            for (int i = 0; i<9000; i+=100)
            {
                if(i%1000 == 0)Console.Write("|");
             
                a = i;
                b = 1000;
                list = new PriorytyQueueUnorderedList<int>();
                if(i<8999)writer.Write(ResultOfTest(0) + ", ");
                else writer.Write(ResultOfTest(0) + "]\n");
            }
            writer.Write("[");
            Console.Write("\n Generowanie danych dla drzewa: \n");
            for (int i = 0; i < 9000; i+=100)
            {
                if (i % 1000 == 0) Console.Write("|");
                a = i;
                b = 1000;
                tree = new PriorityQueueTournamentTree<int>();
                if (i < 8999) writer.Write(ResultOfTest(1) + ", ");
                else writer.Write(ResultOfTest(1) + "]\n");
            }
            Console.Write("\n");
            a = tmpA;
            b = tmpB;
            writer.Close();

        }


    }
}
