Całkowity czas symulacji: 10000
Liczba dostępnych kanałów centrali telefonicznej: 40
Liczba strumieni połączeń: 2
Średnia zajętość wiązki: 39,9987759241618
Średnia zajętość kolejki: 998,603986240368
Parametry strumienia ST1
Prawdopodobieństwo utraty połączenia: 0,995159194770927
Średni czas połączenia: 99,3251368409112
Parametry strumienia ST2
Prawdopodobieństwo utraty połączenia: 0,995025349654733
Średni czas połączenia: 103,168938246272
