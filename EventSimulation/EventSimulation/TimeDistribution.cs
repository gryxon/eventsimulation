﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation
{
    class TimeDistribution
    {
        private Random generator = new Random();

        public double getNumber(double lambda) //Rozkład wykładniczy.
        {
            double t = generator.NextDouble(); //Od ilu procent jest mniejszy?
            double result = (-1)*(1 / lambda) * Math.Log(1 - t); // odwrotność dystrybuanty (f. odwrotna)
            return result;
        }
    }
}
