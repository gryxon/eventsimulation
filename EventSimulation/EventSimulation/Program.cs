﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation
{
    class Program
    {
        private static Simulation simulation;

        private static string[] disName;
        private static double[] lambda;

        private static string[] inSrc;
        private static string[] outSrc;
        private static double timeOfSimulation;
        private static int numberOfFiles;

        static void ReadAndMake(string fileName, int index)
        {
            StreamReader reader = new StreamReader(fileName);
            String trash;
            String[] tmp;
            int channels;
            int queueSize;
            int numberOfDis;
            int numberOfStream;
            trash = reader.ReadLine();

            // Wczytywanie liczby kanalow.
            tmp = (reader.ReadLine()).Split(' ');
            channels = int.Parse(tmp[2]);
            // Wczytanie maksymalnego romiaru kolejki.
            tmp = (reader.ReadLine()).Split(' ');
            queueSize = int.Parse(tmp[2]);
            // Wczytanie liczby rozkładów.
            tmp = (reader.ReadLine()).Split(' ');
            numberOfDis = int.Parse(tmp[2]);

            disName = new string[numberOfDis];
            lambda = new double[numberOfDis];

            for(int i = 0; i<numberOfDis; ++i)
            {
                tmp = (reader.ReadLine()).Split(' ');
                disName[i] = tmp[2];
                tmp = (reader.ReadLine()).Split(' ');
                lambda[i] = double.Parse(tmp[2]);
            }

            tmp = (reader.ReadLine()).Split(' '); 
            numberOfStream = int.Parse(tmp[2]);

            Stream[] stream = new Stream[numberOfStream];

            for(int i = 0; i<numberOfStream; ++i)
            {
                tmp = (reader.ReadLine()).Split(' ');
                string nameStream;
                int reqChannels;
                string nameDisBet;
                string nameDisConn;
                double lambdaBet;
                double lambdaConn;
                double timeWaiting;

                nameStream = tmp[2];
                reqChannels = int.Parse(tmp[5]);
                timeWaiting = int.Parse(tmp[8]);
                nameDisConn = tmp[11];
                nameDisBet = tmp[14];
                lambdaConn = 0;
                lambdaBet = 0;
                for(int j = 0; j<numberOfDis; ++j)
                {
                    if (disName[j] == nameDisConn) lambdaConn = lambda[j];
                    if (disName[j] == nameDisBet) lambdaBet = lambda[j];
                }
                stream[i] = new Stream(nameStream, lambdaBet, lambdaConn, timeWaiting, reqChannels);
            }
            reader.Close();
            
            simulation = new Simulation(timeOfSimulation, stream, channels, queueSize);
            simulation.MakeSimulation();
            simulation.writeResultOfSimulationToFile(outSrc[index]);
            Console.Write("Symulacja nr " + (index+1) + " Zakonczyla sie \n");
        }  

        static void Main(string[] args)
        {
            Console.Write("Proszę podać czas trwania symulacji. \n");
                timeOfSimulation = double.Parse(Console.ReadLine());
            Console.Write("Proszę podać liczbę plików wejściowych.\n");
                numberOfFiles = Convert.ToInt32(Console.ReadLine());
            inSrc = new string[numberOfFiles];
                outSrc = new string[numberOfFiles];
            Console.Write("Proszę podać nazwy plików wejściowych. (w oddzielnych liniach) \n");
                for (int i = 0; i < numberOfFiles; ++i) inSrc[i] = Console.ReadLine();
            Console.Write("Proszę podać nazwy plików wyjściowych. (w oddzielnych liniach) \n");
                for (int i = 0; i < numberOfFiles; ++i) outSrc[i] = Console.ReadLine();

            for (int i = 0; i < numberOfFiles; ++i) ReadAndMake(inSrc[i],i);
        }
    }
}
