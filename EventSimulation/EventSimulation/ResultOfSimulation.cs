﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation
{
    class ResultOfSimulation
    {
        private double[] avarageInServiceTime;
        private double[] probabilityOfLost;
        private double avarageOccupancyOfChannels;
        private double avarageOccupancyOfQueue;

        public ResultOfSimulation(double avarageQueue, double avarageChannels,double[] probOfLost, double[] avarageTime)
        {
            this.avarageInServiceTime = avarageTime;
            this.avarageOccupancyOfChannels = avarageChannels;
            this.avarageOccupancyOfQueue = avarageQueue;
            this.probabilityOfLost = probOfLost;
        }

        public double[] AvarageInServiceTime
        {
            get { return avarageInServiceTime; }
        }
        public double[] ProbabilityOfLost
        {
            get { return probabilityOfLost; }
        }
        public double AvarageOccupancyOfChannels
        {
            get { return avarageOccupancyOfChannels; }
        }
        public double AvarageOccupancyOfQueue
        {
            get { return avarageOccupancyOfQueue; }
        }
    }
}
