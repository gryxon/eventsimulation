﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//DO POPRAWY.

namespace EventSimulation
{
    class SimulationFIFO
    {
        private class ElemFIFO
        {
           public int stream;
           public bool exist;
           public long id;

           public ElemFIFO(int stream = 0, bool exist = false, long id = -1)
            {
                this.stream = stream;
                this.exist = exist;
                this.id = id;
            }
        }

        private ElemFIFO[] queue;
        private int currentFirst;
        private int currentEnd;
        private int currentSize;
        private int maxSize;
        private int realSize;

        private void CorrectFirst()
        {
            if(currentSize != 0)
                while (queue[currentFirst].exist == false) ++currentFirst;
        }

        public SimulationFIFO(int sizeOfBuffor)
        {
            queue = new ElemFIFO[3 * sizeOfBuffor];
            currentFirst = -1;
            currentEnd = 0;
            currentSize = 0;
            maxSize = sizeOfBuffor;
            realSize = 3 * sizeOfBuffor;
            
        }

        private void Clear()
        {
            ElemFIFO[] tempQueue = new ElemFIFO[realSize];
            for(int i = 0, j=0; i<realSize; ++i)
            {
                if(queue[i] != null && queue[i].exist == true)
                {
                    tempQueue[j] = queue[i];
                    ++j;
                }
            }
            queue = tempQueue;
            currentEnd = currentSize;
            currentFirst = 0;
        }

        public bool Add(int stream, long id)
        {
            if (currentSize < maxSize)
            {
                queue[currentEnd] = new ElemFIFO(stream, true, id);
                ++currentEnd;
                ++currentSize;
                if (currentFirst == -1) ++currentFirst;
                if (currentEnd >= realSize-1)Clear();
                return true;
            }
            else return false;
        }

        public bool Remove(int index, long id)
        {
            if (queue[index] != null && queue[index].exist == true && queue[index].id == id)
            {
                queue[index].exist = false;
                --currentSize;
                if (currentSize != 0)CorrectFirst();
                return true;
            }
            else return false; 
        }

        public int CurrentSize
        {
            get { return currentSize; }
        }
       
        public int CurrentFirst
        {
            get { return queue[currentFirst].stream; }
        }

        public int CurrentLast
        {
            get { return currentEnd - 1; }
        }

        public void RemoveFirst()
        {
            queue[currentFirst].exist = false;
            ++currentFirst;
            --currentSize;
            CorrectFirst();
        }

    }
}
