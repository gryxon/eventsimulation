﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation
{
    class Node<T>
    {
        private long id;
        private double rank;
        private T elem;

        public Node(long id, double rank, T elem) {
            this.id = id;
            this.rank = rank;
            this.elem = elem;
        }

        public long Id{
            get { return id; }
        }

        public double Rank
        {
            get { return rank; }
        }

        public T Elem
        {
            get { return elem; }
        }

    }
}
