﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation
{
    class PriorityQueueTournamentTree<T> : PriorityQueue<T>
    {
        private long currentId; // Zmienna umożliwiająca każdemu elementowi przydzielenie konkretnego identyfikatora.
        private int numberOfElem;
        private Node<T>[] nodes; // Właściwe elementy drzewa
        private int[] tree; // Drzewo przechowuje tylko indeksy do elementów. 0 - wartownik, 1 - korzeń. 
        private T guardian;

        public PriorityQueueTournamentTree()
        {
            currentId = 0;
            numberOfElem = 0;
            nodes = new Node<T>[1048576 + 2]; // Maksymalna liczba elementów w kolejce.
            tree = new int[2097152 + 1];  // oraz najmniejsza potęga dwójki większa od niej + 1.

            for (int i = 0; i < 2097153; ++i) tree[i] = 1048576;  // Wstawiamy wartownika

            for (int i = 0; i < 1048577; ++i) nodes[i] = new Node<T>(Int64.MaxValue, Int32.MaxValue, guardian);
        }

        public void InitQueue(T[] elems, double[] ranks)
        { // Narazie powolne O(nlogn), da się w O(2n).
            for (int i = 0; i < ranks.Length; ++i)
            {
                InsertElement(elems[i], ranks[i]);
            }
        }

        public void InsertElement(T elem, double rank)
        {
            nodes[numberOfElem] = new Node<T>(currentId++, rank, elem);

            int tempCurrentTreeIndex = 1048576 + numberOfElem;
            tree[tempCurrentTreeIndex] = numberOfElem;

            while (tempCurrentTreeIndex > 1)
            {
                int leftSon;
                int rightSon;
                if (tempCurrentTreeIndex % 2 == 0)
                {
                    leftSon = tree[tempCurrentTreeIndex];
                    rightSon = tree[tempCurrentTreeIndex + 1];
                }
                else
                {
                    leftSon = tree[tempCurrentTreeIndex - 1];
                    rightSon = tree[tempCurrentTreeIndex];
                }

                tempCurrentTreeIndex /= 2;

                if (compare(leftSon, rightSon) == true) tree[tempCurrentTreeIndex] = leftSon;
                else tree[tempCurrentTreeIndex] = rightSon;
            }

            ++numberOfElem;
        }

        public T GetFirst()
        {
            return nodes[tree[1]].Elem;
        }

        public T EraseFirst()
        {
            T result = nodes[tree[1]].Elem;
            if (tree[1] == numberOfElem - 1)
            {
                --numberOfElem;
                InsertElement(guardian, Int32.MaxValue);
                --numberOfElem;
            }
            else
            {
                int tempFirst = tree[1];

                double tempRankLast = nodes[numberOfElem - 1].Rank;
                long tempIdLast = nodes[numberOfElem - 1].Id;
                long tempId = currentId;

                int tempMax = numberOfElem;
                Swap(ref nodes[tree[1]], ref nodes[numberOfElem - 1]);
                --numberOfElem;
                InsertElement(guardian, Int32.MaxValue);
                --numberOfElem;
                numberOfElem = tempFirst;
                currentId = tempIdLast;
                InsertElement(nodes[tempFirst].Elem, tempRankLast);
                numberOfElem = tempMax - 1;
                currentId = tempId;
            }
            return result;
        }

        public double GetFirstKey()
        {
            return nodes[tree[1]].Rank;
        }


        private bool compare(int indexOfElement1, int indexOfElement2)
        {

            if (nodes[indexOfElement1].Rank != nodes[indexOfElement2].Rank)
                return nodes[indexOfElement1].Rank < nodes[indexOfElement2].Rank;

            else
            {
                return nodes[indexOfElement1].Id <= nodes[indexOfElement2].Id;
            }

        }

        private void Swap<K>(ref K lhs, ref K rhs)
        {
            K temp;
            temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

    }
}
