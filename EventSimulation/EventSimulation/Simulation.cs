﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation
{
    class Simulation
    {
        private enum EventType { ARRIVAL, REJECTION, DEPARTURE };

        private ResultOfSimulation result;
        private double totalTime;
        private Stream[] stream;
        private int maxChannels;
        private int maxQueue;
        
        public Simulation(double totalTime, Stream[] stream, int maxChannels, int maxQueue)
        {
            this.stream = stream;
            this.maxChannels = maxChannels;
            this.totalTime = totalTime;
            this.maxQueue = maxQueue;
        }

        public void MakeSimulation()
        {
            int freeChannels = maxChannels;
            TimeDistribution getterTime = new TimeDistribution();
            PriorityQueueTournamentTree<Event> eventTree = new PriorityQueueTournamentTree<Event>();
            SimulationFIFO queue = new SimulationFIFO(maxQueue);

            double currentTime = 0.0;
            double sumChannels = 0.0; //Zmimenna pomocnicza potrzebna do obliczenia średniej zajętości wiązki.
            double sumQueue = 0.0; // Zmienna pomocnicza potrzebna do obliczenia średniej zajętości kolejki.
            double previousTime = 0.0;

            // Talbice pomocnicze pomagajace wyznaczyc prawdopodobienstwo odrzucenia pakietu.
            long[] goodAsk = new long[stream.Length]; // Ale tylko te odrzucone lub dobre.  
            long[] badAsk = new long[stream.Length];
            double[] timeOfConnection = new double[stream.Length];
            // goodAsk obliczamy ze wzoru goodAsk = allAsk - badAsk.



            for (int i = 0; i < stream.Length; ++i)
            {
                Event newEvent = new Event(-1, i, (int)EventType.ARRIVAL);
                eventTree.InsertElement(newEvent, currentTime + getterTime.getNumber(stream[i].TimeBetween));
                goodAsk[i] = 0;
                badAsk[i] = 0;
                timeOfConnection[i] = 0.0;
            }



            while (currentTime < totalTime)
            {
                currentTime = eventTree.GetFirstKey(); // pobranie aktualnego czasu. 
                Event currentEvent = eventTree.EraseFirst();


              
                if (previousTime != currentTime)
                {
                    sumChannels += (maxChannels - freeChannels)*(currentTime-previousTime);
                    sumQueue += queue.CurrentSize*(currentTime - previousTime);
                }

                switch (currentEvent.Type)
                {
                    case (int)EventType.ARRIVAL:
                        if (queue.CurrentSize == 0 && stream[currentEvent.NumberOfStream].RequiredChannels <= freeChannels)
                        {
                            freeChannels -= stream[currentEvent.NumberOfStream].RequiredChannels;
                            Event departure = new Event(-1, currentEvent.NumberOfStream, (int)EventType.DEPARTURE, currentTime);
                            eventTree.InsertElement(departure, currentTime + getterTime.getNumber(stream[currentEvent.NumberOfStream].TimeOfConnection));
                        }

                        else
                        {
                            if (queue.Add(currentEvent.NumberOfStream, currentEvent.Id))
                            {
                                Event rejection = new Event(queue.CurrentLast, currentEvent.NumberOfStream, (int)EventType.REJECTION);
                                eventTree.InsertElement(rejection, currentTime + stream[currentEvent.NumberOfStream].MaxTimeOfWaiting);
                            }
                            else
                            {
                                badAsk[currentEvent.NumberOfStream] += 1;
                            }
                        }

                        Event newEvent = new Event(-1, currentEvent.NumberOfStream, (int)EventType.ARRIVAL);
                        eventTree.InsertElement(newEvent, currentTime + getterTime.getNumber(stream[currentEvent.NumberOfStream].TimeBetween));

                        break;

                    case (int)EventType.REJECTION:
                        bool boolResult = queue.Remove(currentEvent.QueueIndex, currentEvent.Id);
                        if (boolResult)
                        {
                            badAsk[currentEvent.NumberOfStream] += 1; //Jeżeli obiekt został obslużony wcześniej.
                        }

                        break;

                    case (int)EventType.DEPARTURE:
                        freeChannels += stream[currentEvent.NumberOfStream].RequiredChannels; //zwolnienie kanałów.
                        goodAsk[currentEvent.NumberOfStream] += 1;
                        timeOfConnection[currentEvent.NumberOfStream] += (currentTime - currentEvent.ArrivalTime);
                        
                        while (queue.CurrentSize >= 1 && stream[queue.CurrentFirst].RequiredChannels <= freeChannels)
                        {
                            Event departure = new Event(-1, queue.CurrentFirst, (int)EventType.DEPARTURE, currentTime);
                            eventTree.InsertElement(departure, currentTime + getterTime.getNumber(stream[queue.CurrentFirst].TimeOfConnection));
                            freeChannels -= stream[queue.CurrentFirst].RequiredChannels;
                            queue.RemoveFirst();
                        }

                        break;

                }

                previousTime = currentTime;
            }

            double[] avarageInServiceTime = new double[stream.Length];
            double[] probOfLost = new double[stream.Length];

            for (int i = 0; i < stream.Length; ++i)
            {
                avarageInServiceTime[i] = (double)timeOfConnection[i] / (double)goodAsk[i];
                probOfLost[i] = (double)badAsk[i] / ((double)badAsk[i] + (double)goodAsk[i]);
            }

            result = new ResultOfSimulation((double)sumQueue / totalTime, (double)sumChannels / totalTime,
                probOfLost, avarageInServiceTime);
        }

        public void writeResultOfSimulation()
        {
            Console.Write("Całkowity czas symulacji: "+totalTime+ "\n");
            Console.Write("Liczba dostępnych kanałów centrali telefonicznej: " + maxChannels + "\n");

            Console.Write("Liczba strumieni połączeń: " + stream.Length + "\n");
            Console.Write("Średnia zajętość wiązki: " +result.AvarageOccupancyOfChannels+ "\n");
            Console.Write("Średnia zajętość kolejki: " + result.AvarageOccupancyOfQueue + "\n \n");

            for(int i = 0; i<stream.Length; ++i)
            {
                Console.Write("Parametry strumienia " + stream[i].Name + "\n");
                Console.Write("Prawdopodobieństwo utraty połączenia: " + result.ProbabilityOfLost[i] + "\n");
                Console.Write("Średni czas połączenia: " + result.AvarageInServiceTime[i] + "\n");
            }
        }

        public void writeResultOfSimulationToFile(string nameFile)
        {
            string[] tmp = { " " };
            File.WriteAllLines(nameFile, tmp); //Czyszczenie pliku przed zapisem.
            FileStream fs = new FileStream(nameFile, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter writer = new StreamWriter(fs);
            writer.WriteLine("Całkowity czas symulacji: " + totalTime);
            writer.WriteLine("Liczba dostępnych kanałów centrali telefonicznej: " + maxChannels);

            writer.WriteLine("Liczba strumieni połączeń: " + stream.Length);
            writer.WriteLine("Średnia zajętość wiązki: " + result.AvarageOccupancyOfChannels);
            writer.WriteLine("Średnia zajętość kolejki: " + result.AvarageOccupancyOfQueue);

            for (int i = 0; i < stream.Length; ++i)
            {
                writer.WriteLine("Parametry strumienia " + stream[i].Name);
                writer.WriteLine("Prawdopodobieństwo utraty połączenia: " + result.ProbabilityOfLost[i] );
                writer.WriteLine("Średni czas połączenia: " + result.AvarageInServiceTime[i]);
            }
            writer.Close();
        }

    }
}
