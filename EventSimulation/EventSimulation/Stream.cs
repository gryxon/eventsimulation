﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation
{
    class Stream
    {
        private double lambdaTimeBetween;
        private double lambdaTimeOfConnection;
        private double maxTimeOfWaiting;
        private int requiredChannels;
        private string name;
        private Random generator = new Random();

        public Stream(string name, double lambdaTimeBetween, double lambdaTimeOfConnection, double maxTimeOfWainting, int requiredChannels)
        {
            this.lambdaTimeBetween = lambdaTimeBetween;
            this.lambdaTimeOfConnection = lambdaTimeOfConnection;
            this.maxTimeOfWaiting = maxTimeOfWainting;
            this.requiredChannels = requiredChannels;
            this.name = name;
        }
        
        public double TimeBetween
        {
            get { return lambdaTimeBetween; }
        }

        public double TimeOfConnection
        {
            get { return lambdaTimeOfConnection; }
        }

        public double MaxTimeOfWaiting
        {
            get { return maxTimeOfWaiting; }
        }

        public int RequiredChannels
        {
            get { return requiredChannels; }
        }

        public string Name
        {
            get { return name; }
        }

    }
}
