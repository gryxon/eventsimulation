﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventSimulation
{
    class Event
    {
        private enum EventType { ARRIVAL, REJECTION, DEPARTURE };

        private static long currentId = 0;

        private int queueIndex; // FOR REJECTION
        private int numberOfStream;
        private long id;
        private int type; // 0 - ARRIVAL, 1 - REJECTION, 2 - DEPARTURE
        private double arrivalTime; // For Departure. 

        public Event(int queueIndex, int numberOfStream, int type)
        {
            this.queueIndex = queueIndex;
            this.numberOfStream = numberOfStream;
            this.type = type;
            this.id = currentId++;

        }

        public Event(int queueIndex, int numberOfStream, int type, double arrivalTime)
        {
            this.queueIndex = queueIndex;
            this.numberOfStream = numberOfStream;
            this.type = type;
            this.id = currentId++;
            this.arrivalTime = arrivalTime;

        }

        public int QueueIndex
        {
            get { return queueIndex; }
        }

        public int NumberOfStream
        {
            get { return numberOfStream; }
        }

        public int Type
        {
            get { return type; }
        }

        public long Id
        {
            get { return id; }
        }
        public double ArrivalTime
        {
            get { return arrivalTime; }
        }
    }
}
